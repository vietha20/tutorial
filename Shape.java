/*
  klasser är ett abstrakt sätt att beskriva saker som finns 
  IRL. 
  
  klasser ska vara publika (minst en utav)

 */

public class Shape {

    //private variabeln kan bara kommas åt från alla funktioner från klassen Shape
    private String test_string = "hello";

    //publik variabeln kan kommas åt från alla funktion inom klassen och även utanför klassen
    String test_string_1 = "hej";
    
    //protected funktion: får inte ändrar värdet
    protected double caclArea(int width, int height) {
	return width*height;
    }

    //args är en lista av argument som skickas in när programmet körs
    public static void main(String[] args) {

	//instanser av rektangel och triangel
	Triangle tri = new Triangle();
	Rectangle rect = new Rectangle();

	//lokala variabler
	int width = 10;
	int height = 20;
	
	tri.calcArea(width, height);
	rect.calcArea(width, height);	
    }
}

//subklass ärver funktioner från huvudklassen Shape
class Rectangle extends Shape {
    double calcArea(int width, int height) {
	System.out.println("width: " + width + " height: " + height + " area: " + width*height);
	return width*height;
    }
}


//subklass ärver funktioner från huvudklassen Shape
class Triangle extends Shape {
    double calcArea(int width, int height) {
	System.out.println("width: " + width + " height: " + height + " area " + ((width*height)/2));
	return (width*height)/2;
    }
}
